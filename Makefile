check: black pylint

black:
	isort --profile black lavafed/ manage.py
	LC_ALL=C.UTF-8 LANG=C.UTF-8 black submit lavafed/ manage.py tests/

pylint:
	pylint --disable=all --enable=elif,exceptions,stdlib,imports,variables,string,string_constant,logging,newstyle,classes --disable=C0411,C0412,E0401,E0611,E0202,R0201,R0401,W --enable=E0102,R1707,R1714,R1715,R1716,W0235,W0404,W0611,W0612,W0622,W0631,W0632,W0641,W0642,W1401,W1402,W1403,W1505 submit lavafed/ manage.py tests/

test:
	python3 -m pytest -v --ds lavafed.settings.dev --cov lavafed --cov-report html --cov-report term-missing --cov-report=xml:coverage.xml --junitxml=lavafed.xml tests/

build:
	echo "$$(git describe)" > lavafed/VERSION
	lambpy -r requirements-aws.txt -i lavafed

run:
	python3 manage.py runserver


