.. _installation:

Installation
############

Docker
======

A docker container is provided at **hub.lavasoftware.org/lava/lavafed**

.. code-block:: shell

    docker container run -p 9001:80 \
      "hub.lavasoftware.org/lava/lavafed:latest"

Visit http://127.0.0.1:80/ to browse your local lavafed.

Development versions
=====================

It's also possible to use lavafed directly from the sources:

.. code-block:: shell

    git clone https://git.lavasoftware.org/lava/lavafed.git
    cd lavafed
    virtualenv -p python3 venv
    source venv/bin/activate
    python3 -m pip install -r requirements.txt
    django-admin startproject fedweb .
    cp share/urls.py fedweb/urls.py
    cp share/settings.py fedweb/custom_settings.py
    echo "INSTALLED_APPS.append(\"lavafed\")" >> fedweb/settings.py
    echo "from fedweb.custom_settings import *" >> fedweb/settings.py
    python3 manage.py migrate
    DEBUG=1 python3 manage.py runserver

Visit http://127.0.0.1:8000/ to browse your development environment.

Official instance
=================

The lavafed official instance, hosted by the lavasoftware project is made of
two docker containers:

* ``hub.lavasoftware.org/lava/lavafed``
* ``hub.lavasoftware.org/lava/lava/amd64/lava-server``

lavafed
-------

The lavafed application is started by Gitlab as part of the CI with:

.. code-block:: shell

    docker container run --name lavafed-web -d \
      --restart always \
      -p 9001:80 \
      -v "/home/lavafed/db.sqlite3:/app/db.sqlite3" \
      -v "/home/lavafed/etc/labs/:/app/etc/labs/" \
      -v "/home/lavafed/etc/master.yaml:/app/etc/master.yaml" \
      "hub.lavasoftware.org/lava/lavafed:latest"

The main apache2 server is configured to proxy ``https://federation.lavasoftware.org/``
to this container.

federated lava server
---------------------

The federated lava server is started by Gitlab as part of the CI with:

.. code-block:: shell

    docker run --name lavafed-master --rm -d \
      -p 9000:80 -p 6500:5500 -p 6555:5555 -p 6556:5556 \
      -v "/home/lavafed/lava-master/certificates.d/:/etc/lava-dispatcher/certificates.d/" \
      -v "/home/lavafed/lava-master/instance.conf:/etc/lava-server/instance.conf" \
      -v "/home/lavafed/lava-master/lava-logs:/etc/lava-server/lava-logs" \
      -v "/home/lavafed/lava-master/lava-master:/etc/lava-server/lava-master" \
      -v "/home/lavafed/lava-master/lava-server.conf:/etc/apache2/sites-enabled/lava-server.conf" \
      -v "/home/lavafed/lava-master/settings.conf:/etc/lava-server/settings.conf" \
      -v "lavafed-master-db:/var/lib/postgresql/9.6/main/" \
      -v "lavafed-master-jobs:/var/lib/lava-server/default/media/" \
      "hub.lavasoftware.org/lava/lava/amd64/lava-server:2019.05.0012.g7d4454923"

The main apache2 server is configured to proxy ``https://federation.lavasoftware.org/lava/``
to this container.

Configuration
#############

lavafed
=======

XXXX

federated lava server
=====================

User
----

The federated lava admin should create a user (called ``lavafed`` in this
document) that will be used by the `acquire` and `release` commands. This user
should have the rights to create device-types, devices and workers and to
submit jobs.

This is translating to the following permissions:

.. code-block:: text

    lava_scheduler_app | device | Can add device
    lava_scheduler_app | device | Can change device
    lava_scheduler_app | device type | Can add device type
    lava_scheduler_app | device type | Can change device type
    lava_scheduler_app | tag | Can add tag
    lava_scheduler_app | test job | Can add test job
    lava_scheduler_app | test job | Can cancel/resubmit job
    lava_scheduler_app | worker | Can change worker

Tokens
------

The `acquire` and `release` commands need a token to authenticate as the
lavafed user for the XMLRPC API. Federated lava Admin can create such token in
the lava admin interface.

This token will be called ``lava_server_lavafed_token`` in the rest of the
documentation.

A second token should be created to hold the lavafed api token used by the
federated lava server when submitting results to the lavafed api.

.. code-block:: yaml

    description: federation.lavasoftware.org
    secret: {{ federated_lavafed_api_token }}

.. note:: this token should be created when a first remote lab is added.

zmq certificates
----------------

The zmq channel between remote dispatchers and the federated master is encrypted.

In order to encrypt the communication, the federated lava admin should generate
a certificate:

.. code-block:: shell

    mkdir data
    docker run --rm \
      -v "/home/lavafed/lava-master/certificates.d/:/data" \
      "hub.lavasoftware.org/lava/lava/amd64/lava-server:2019.05.0012.g7d4454923" \
      /usr/share/lava-common/create_certificate.py \
        --directory /data master

The public (``master.key``) and private (``master.key_secret``) keys will be
created in ``/home/lavafed/lava-master/certificates.d/``.

The value of the public key, is called ``lava_master_zmq_public_certificate``
in the rest of the documentation.

master.yaml
-----------

The lavafed master configuration in `etc/master.yaml` should look like:

.. code-block:: yaml

    # base job url of the web interface
    job_url: https://federation.lavasoftware.org/lava/scheduler/job/{id}

    # xmlrpc configuration
    # The user should be able to administrate devices and submit jobs
    url: https://federation.lavasoftware.org/lava/RPC2/
    username: lavafed
    token: "{{ lava_server_lavafed_token }}"

    # zmq urls to the lava-master and lava-slave
    master: tcp://federation.lavasoftware.org:6556
    logs: tcp://federation.lavasoftware.org:6555
    certificates:
      public: "{{ lava_master_zmq_public_certificate }}"

    # api configuration
    api:
      url: https://federation.lavasoftware.org/api/v0.1/jobs/
      token: federation.lavasoftware.org
