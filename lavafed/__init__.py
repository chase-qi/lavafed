from pathlib import Path

BASE = (Path(__file__) / "..").resolve()

if (BASE / "VERSION").exists():
    __version__ = (BASE / "VERSION").read_text(encoding="utf-8").rstrip("\n")
else:
    __version__ = "git"
