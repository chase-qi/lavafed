# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from django.contrib import admin

from .models import DeviceType, Feature, Job, Lab, Token, Version


class DeviceTypeAdmin(admin.ModelAdmin):
    ordering = ["name"]


class FeatureAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "type", "action")
    list_filter = ("name", "type", "action")
    ordering = ["name"]


class JobAdmin(admin.ModelAdmin):
    list_display = ("name", "dt", "lab", "version", "type", "health", "lava")
    list_filter = ("type", "health", "version")


class LabAdmin(admin.ModelAdmin):
    list_display = ("name", "url")
    ordering = ["name"]


class TokenAdmin(admin.ModelAdmin):
    list_display = ("lab", "description")
    list_filter = ("lab",)
    ordering = ["lab__name"]


admin.site.register(DeviceType, DeviceTypeAdmin)
admin.site.register(Feature, FeatureAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(Lab, LabAdmin)
admin.site.register(Token, TokenAdmin)
admin.site.register(Version)
