# Generated by Django 1.11.16 on 2018-10-16 13:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Device",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=200)),
                ("acquired", models.BooleanField(default=False)),
                (
                    "health",
                    models.IntegerField(
                        choices=[
                            (0, "Good"),
                            (1, "Unknown"),
                            (2, "Looping"),
                            (3, "Bad"),
                            (4, "Maintenance"),
                            (5, "Retired"),
                        ],
                        default=1,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="DeviceType",
            fields=[
                (
                    "name",
                    models.CharField(max_length=200, primary_key=True, serialize=False),
                )
            ],
        ),
        migrations.CreateModel(
            name="Job",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("lava", models.IntegerField(verbose_name="Lava job id")),
                (
                    "type",
                    models.IntegerField(
                        choices=[(0, "FED"), (1, "SLAVE"), (2, "META")], default=0
                    ),
                ),
                (
                    "health",
                    models.IntegerField(
                        choices=[
                            (0, "Unknown"),
                            (1, "Complete"),
                            (2, "Incomplete"),
                            (3, "Canceled"),
                        ],
                        default=0,
                    ),
                ),
                (
                    "device",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="lavafed.Device"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Lab",
            fields=[
                (
                    "name",
                    models.CharField(max_length=200, primary_key=True, serialize=False),
                )
            ],
        ),
        migrations.CreateModel(
            name="Version",
            fields=[
                (
                    "version",
                    models.CharField(max_length=200, primary_key=True, serialize=False),
                )
            ],
        ),
        migrations.AddField(
            model_name="job",
            name="version",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="lavafed.Version"
            ),
        ),
        migrations.AddField(
            model_name="device",
            name="dt",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="lavafed.DeviceType"
            ),
        ),
        migrations.AddField(
            model_name="device",
            name="lab",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="lavafed.Lab"
            ),
        ),
        migrations.AddIndex(
            model_name="device",
            index=models.Index(
                fields=["name", "dt", "lab"], name="lavafed_dev_name_0e5582_idx"
            ),
        ),
        migrations.AlterUniqueTogether(
            name="device", unique_together={("name", "dt", "lab")}
        ),
    ]
