# Generated by Django 1.11.20 on 2019-03-12 15:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("lavafed", "0007_update_job")]

    operations = [
        migrations.CreateModel(
            name="Feature",
            fields=[
                (
                    "name",
                    models.CharField(max_length=200, primary_key=True, serialize=False),
                ),
                ("description", models.TextField(blank=True, null=True)),
                (
                    "type",
                    models.IntegerField(
                        choices=[(0, "Generic"), (1, "Device specific")], default=0
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="job",
            name="features",
            field=models.ManyToManyField(blank=True, to="lavafed.Feature"),
        ),
    ]
