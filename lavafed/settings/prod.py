import contextlib
import os
from pathlib import Path

import environ
import yaml
from django.core.management.utils import get_random_secret_key

from lavafed import __version__
from lavafed.settings.common import *

env = environ.Env(
    DEBUG=(bool, False),
    SENTRY_DSN=(str, None),
)
environ.Env.read_env()

DEBUG = env("DEBUG")
SECRET_KEY = env("SECRET_KEY")

DATABASES = {
    "default": env.db(),
}

# Also include admin static files without running collectstatic
WHITENOISE_USE_FINDERS = True

# Enable sentry
SENTRY_DSN = env("SENTRY_DSN")

# Add sentry if SENTRY_DSN is defined
if SENTRY_DSN:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration

    sentry_sdk.init(
        dsn=SENTRY_DSN,
        environment="prod",
        integrations=[DjangoIntegration()],
        release=f"lavafed@{__version__}",
    )
