# vim: set ts=4

# Copyright 2018 Linaro Ltd
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from django.contrib import admin
from django.urls import path

from lavafed import views as v

urlpatterns = [
    path("", v.home, name="home"),
    path("admin/", admin.site.urls),
    path("api/v0.1/jobs/", v.api_v0_1_jobs, name="api.v0.1.jobs"),
    path(
        "api/v0.1/versions/<version>/",
        v.api_v0_1_versions_show,
        name="api.v0.1.versions.show",
    ),
    path("device-types/", v.device_types, name="device_types"),
    path("device-types/<str:name>/", v.device_type_show, name="device_types.show"),
    path("labs/", v.labs, name="labs"),
    path("labs/<str:lab>/", v.lab_show, name="labs.show"),
    path("versions/", v.versions, name="versions"),
    path("versions/latest/", v.version_show_latest, name="versions.show_latest"),
    path("versions/<str:version>/", v.version_show, name="versions.show"),
    path(
        "versions/latest/badge/",
        v.version_badge_latest,
        name="versions.badge_latest",
    ),
    path("versions/<version>/badge/", v.version_badge, name="versions.badge"),
]
