# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import xmlrpc.client
from urllib.parse import urlparse

import jinja2
import requests

from lavafed import __version__


#######
# Class
#######
class RequestsTransport(xmlrpc.client.Transport):
    def __init__(self, scheme):
        super().__init__()
        self.scheme = scheme

    def request(self, host, handler, request_body, verbose=False):
        headers = {
            "User-Agent": f"lavafed %{__version__}",
            "Content-Type": "text/xml",
            "Accept-Encoding": "gzip",
        }
        url = "%s://%s%s" % (self.scheme, host, handler)
        try:
            response = None
            response = requests.post(
                url, data=request_body, headers=headers, timeout=20
            )
            response.raise_for_status()
            return self.parse_response(response)
        except xmlrpc.client.Fault:
            raise
        except Exception as e:
            if response is None:
                raise xmlrpc.client.ProtocolError(url, 500, str(e), "")
            else:
                raise xmlrpc.client.ProtocolError(
                    url, response.status_code, str(e), response.headers
                )

    def parse_response(self, response):
        """
        Parse the xmlrpc response.
        """
        p, u = self.getparser()
        p.feed(response.text)
        p.close()
        return u.close()


#########
# Helpers
#########
def build_proxy(url, username, token):
    (scheme, uri) = build_uri(url, username, token)
    return xmlrpc.client.ServerProxy(
        uri, allow_none=True, transport=RequestsTransport(scheme)
    )


def build_uri(url, username, token):
    p = urlparse(url)
    uri = "%s://%s:%s@%s%s" % (p.scheme, username, token, p.netloc, p.path)
    return (p.scheme, uri)


def render_template(f_template, base_dir, **kwargs):
    env = jinja2.Environment(  # nosec - rendering yaml
        autoescape=False,
        loader=jinja2.FileSystemLoader([str(base_dir)]),
        undefined=jinja2.StrictUndefined,
    )
    data = f_template.read_text(encoding="utf-8")
    template = env.from_string(data)
    try:
        return template.render(**kwargs)
    except jinja2.TemplateNotFound:
        return None
