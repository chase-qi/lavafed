resource "aws_apigatewayv2_api" "api" {
  name                         = local.workspace
  protocol_type                = "HTTP"
  disable_execute_api_endpoint = true

  tags = {
    Name = local.workspace
  }
}

resource "aws_apigatewayv2_domain_name" "api" {
  domain_name = local.default.tld

  domain_name_configuration {
    certificate_arn = module.acm.acm_certificate_arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }
}

resource "aws_apigatewayv2_stage" "api" {
  api_id      = aws_apigatewayv2_api.api.id
  name        = "$default"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.logs.arn
    format          = <<EOF
{"requestId": "$context.requestId", "ip": "$context.identity.sourceIp", "caller": "$context.identity.caller", "user":"$context.identity.user", "requestTime": "$context.requestTime", "httpMethod": "$context.httpMethod", "status": "$context.status", "protocol": "$context.protocol", "responseLength": "$context.responseLength", "error": "$context.error.message", "user-agent": "$context.identity.userAgent", "integration_error": "$context.integration.error" }
EOF
  }

  tags = {
    Name = local.workspace
  }
}

resource "aws_apigatewayv2_api_mapping" "api" {
  api_id      = aws_apigatewayv2_api.api.id
  domain_name = aws_apigatewayv2_domain_name.api.id
  stage       = aws_apigatewayv2_stage.api.id
}

resource "aws_apigatewayv2_route" "api" {
  api_id    = aws_apigatewayv2_api.api.id
  route_key = "$default"
  target    = "integrations/${aws_apigatewayv2_integration.api.id}"
}

resource "aws_apigatewayv2_integration" "api" {
  api_id           = aws_apigatewayv2_api.api.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  integration_method     = "ANY"
  integration_uri        = aws_lambda_function.api.arn
  payload_format_version = "2.0"
}

