resource "aws_cloudwatch_log_group" "logs" {
  name              = "/aws/apigateway/${local.workspace}"
  retention_in_days = 365

  tags = {
    Name = local.workspace
  }
}

resource "aws_cloudwatch_log_group" "api" {
  name              = "/aws/lambda/${local.workspace}-api"
  retention_in_days = 14

  tags = {
    Name = local.workspace
  }
}

