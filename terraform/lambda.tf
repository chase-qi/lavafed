resource "aws_lambda_permission" "apigw" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api.arn
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.api.execution_arn}/*/*"
}

resource "aws_lambda_function" "api" {
  function_name = "${local.workspace}-api"

  s3_bucket = aws_s3_bucket.lambda.id
  s3_key    = "lambda.zip"

  handler          = "lavafed.lambda.handler"
  runtime          = "python3.10"
  source_code_hash = filebase64sha256("${path.module}/../lambda.zip")
  memory_size      = 512
  timeout          = 35

  environment {
    variables = local.lambda_env
  }

  vpc_config {
    subnet_ids         = [aws_default_subnet.default_a.id, aws_default_subnet.default_b.id]
    security_group_ids = [aws_security_group.db.id]
  }

  role       = aws_iam_role.lambda_exec.arn
  depends_on = [aws_cloudwatch_log_group.api, aws_s3_object.lambda]

  tags = {
    Name = local.workspace
  }
}
