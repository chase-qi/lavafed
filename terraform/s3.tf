resource "aws_s3_bucket" "lambda" {
  bucket = "${local.workspace}-lambda"

  tags = {
    Name = local.workspace
  }
}

resource "aws_s3_object" "lambda" {
  bucket      = aws_s3_bucket.lambda.id
  key         = "lambda.zip"
  source      = "${path.module}/../lambda.zip"
  source_hash = filemd5("${path.module}/../lambda.zip")
}
