resource "random_password" "database_password" {
  length  = 16
  special = false
}

resource "random_password" "django_secret_key" {
  length  = 50
  special = true
}

resource "aws_ssm_parameter" "database_url" {
  name        = "/${local.workspace}/database/url"
  description = "${local.workspace} database password"
  type        = "SecureString"
  value       = "postgres://lavafed:${random_password.database_password.result}@${aws_db_instance.db.endpoint}/lavafed?CONN_MAX_AGE=0"

  tags = {
    Name = local.workspace
  }
}

resource "aws_ssm_parameter" "django_secret_key" {
  name        = "/${local.workspace}/django/secret_key"
  description = "django secret key"
  type        = "SecureString"
  value       = random_password.django_secret_key.result

  tags = {
    Name = local.workspace
  }
}
