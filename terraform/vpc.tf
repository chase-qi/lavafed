resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_subnet" "default_a" {
  availability_zone = "${var.region}a"
}

resource "aws_default_subnet" "default_b" {
  availability_zone = "${var.region}b"
}

